/*
 * @lc app=leetcode.cn id=804 lang=java
 *
 * [804] 唯一摩尔斯密码词
 */

import java.util.TreeSet;

// @lc code=start
class Solution {
    public int uniqueMorseRepresentations(String[] words) {
        String[] codes = {".-","-...","-.-.","-..",".","..-.","--.",
        "....","..",".---","-.-",".-..","--","-.","---",
        ".--.","--.-",".-.","...","-","..-","...-",".--",
        "-..-","-.--","--.."};
        
        TreeSet<String> set = new TreeSet<>();
        for(String word: words){

            StringBuilder sb = new StringBuilder();

            for(int i=0;i<word.length();i++){
                sb.append(codes[word.charAt(i) - 'a']);
            }

            set.add(sb.toString());
        }

        return set.size();
    }
}
// @lc code=end

