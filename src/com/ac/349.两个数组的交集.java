import java.util.ArrayList;
import java.util.TreeSet;

/*
 * @lc app=leetcode.cn id=349 lang=java
 *
 * [349] 两个数组的交集
 */

// @lc code=start
class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        TreeSet<Integer> treeSet = new TreeSet<>();
        for(int num: nums1)
            treeSet.add(num);

        ArrayList<Integer> arrayList = new ArrayList<>();
        for(int i =0;i<nums2.length;i++){
            if(treeSet.contains(nums2[i]))
                arrayList.add(nums2[i]);
                treeSet.remove(nums2[i]);
        }

        int [] result = new int[arrayList.size()];
        for(int i=0;i<arrayList.size();i++){
            result[i] = arrayList.get(i);
        }

        return result;
    }
}
// @lc code=end

